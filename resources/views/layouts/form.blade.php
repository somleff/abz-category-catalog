<div class="form-group">
    <label for="name" class="col-md-4 control-label">Name</label>
    <div class="col-md-6">
        <input id="name" type="text" class="form-control" name="name" placeholder="Full Name"
        value="{{ old('name',$employee->name ?? '') }}">
    </div>
</div>

<div class="form-group">
    <label for="position" class="col-md-4 control-label">Position</label>
    <div class="col-md-6">
        <select class="form-control" name="position">
            <option>{{ old('position',$employee->position ?? 'Position') }}</option>
            <option value="ceo">CEO</option>
            <option value="execute_chief">Execute Chief</option>
            <option value="producer">Producer</option>
            <option value="manager">Manager</option>
            <option value="seller">Seller</option>
        </select>

    </div>
</div>

<div class="form-group">
    <label for="employment" class="col-md-4 control-label" >Employment</label>
    <div class="col-md-6">
        <input id="employment" type="text" class="form-control" name="employment" placeholder="Employment"
                value="{{ old('employment',$employee->employment ?? '') }}">
    </div>
</div>

<div class="form-group">
    <label for="salary" class="col-md-4 control-label">Salary</label>
    <div class="col-md-6">
        <input id="salary" type="text" class="form-control" name="salary" placeholder="Salary"
                value="{{ old('salary',$employee->salary ?? '') }}">
    </div>
</div>

<div class="form-group">
    <label for="chief_id" class="col-md-4 control-label">Chief ID</label>
    <div class="col-md-6">
        <input id="chief_id" type="text" class="form-control" name="chief_id" placeholder="Find ID in List :)"
                value="{{ old('chief_id',$employee->chief_id ?? '') }}">
        <!-- <select class="form-control" >
            <option>Choose chief</option>
            <option>CEO</option>
            <option>Execute Chief</option>
            <option>Producer</option>
            <option>Manager</option>
            <option>Seller</option>
        </select> -->
    </div>
</div>

<div class="form-group">
    <div class="col-md-6 col-md-offset-4">
        <button type="submit" class="btn btn-primary">
            Submit
        </button>
        <a class="btn btn-link" href="{{ url('list') }}">
            Back
        </a>
    </div>
</div>
