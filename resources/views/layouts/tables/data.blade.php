<tr>
    <td>{{ $employee->id }}</td>
    <td><a href="/list/{{$employee->id}}">{{ $employee->name }}</a></td>
    <td>{{ $employee->position }}</td>
    <td>{{ $employee->employment }}</td>
    <td>{{ $employee->salary }}</td>
    <td><a class="btn btn-info btn-sm" href="{{ url('list/' . $employee->id . '/edit') }}">Edit</a>
        <form action="{{url('list', [$employee->id])}}" method="POST" style="display:inline">
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <input type="submit" class="btn btn-danger btn-sm" value="Delete"/>
        </form>
    </td>
</tr>
