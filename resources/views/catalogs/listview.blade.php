@extends ('layouts.app', ['title' => 'Employees List'])

@section ('content')
<div class="container">
    <h3 class="text-center">Employees List</h3>
    <div class="panel panel-default table-responsive">

        {{--Flash a success message--}}
        @if (session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
            </div>
        @endif

        <table class="table table-bordered table-hover">
            @include('layouts.tables.header')

            @foreach($employees as $employee)
                @include('layouts.tables.data')
            @endforeach
        </table>

    </div>
</div>

    {{-- Add Pagination To View  --}}
<div class="container">
    <nav aria-label="Page navigation" class="text-center">

        {{ $employees->appends(request()->all())->links() }}

    </nav>
</div>

@endsection
