@extends ('layouts.app', ['title' => 'Serch Result'])

@section ('content')
<div class="container">
    <h3 class="text-center">Serch Result for {{ $query }}</h3>
    <div class="panel panel-default">

        <table class="table table-bordered table-hover tablesorter">
            @include('layouts.tables.header')

            @if(isset($details))
                @foreach($details as $employee)
                    @include('layouts.tables.data')
                @endforeach
            @endif
        </table>

    </div>
</div>

    {{-- Add Pagination To View  --}}
<div class="container">
    <nav aria-label="Page navigation" class="text-center">
        {{ $details->appends(Request::only('query'))->render() }}
    </nav>
</div>

@endsection
