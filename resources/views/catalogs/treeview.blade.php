@extends ('layouts.app', ['title' => 'Employees Tree'])

@section ('content')
<div class="container">
    <h3>Employees Tree</h3>
    <div class="panel panel-default">
        <ul id="tree1">

            @foreach($catalogs as $catalog)
                <li>
                    {{ $catalog->name }}
                    {{ $catalog->position }}

                    @if(count($catalog->childs)>0)
                        @include('catalogs.manageChild',['childs' => $catalog->childs])
                    @endif
                </li>
            @endforeach

        </ul>
    </div>
</div>

@endsection

@section ('script')
<script src="{{asset('js/treeview.js')}}"></script>
@endsection
