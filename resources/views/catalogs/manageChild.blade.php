<ul>
@foreach($childs as $child)
    <li>
        {{ $child->name }}
        {{ $child->position }}
        @if(count($child->childs)>0)
            @include('catalogs.manageChild', ['childs' => $child->childs])
         @endif
    </li>
@endforeach
</ul>
