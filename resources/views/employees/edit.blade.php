@extends ('layouts.app', ['title' => 'Edit Employee'])

@section ('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Edit Employee</div>

                {{-- Check on errors --}}
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                {{-- Form connection --}}
                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ url('list', [$employee->id]) }}">
                        <input type="hidden" name="_method" value="PUT">
                        {{ csrf_field() }}
                        @include('layouts.form')
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>

@endsection
