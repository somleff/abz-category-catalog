@extends ('layouts.app', ['title' => 'Prewiew Employee'])

@section ('content')
<div class="container">
    <h3 class="text-center">Employee {{ $employee->name }} Info</h3>
    <div class="panel panel-default">
        <table class="table table-bordered table-hover tablesorter">

            @include('layouts.tables.header')
            @include('layouts.tables.data')

        </table>
    </div>
</div>

@endsection
