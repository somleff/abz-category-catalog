<?php

use Illuminate\Database\Seeder;

class CatalogsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'ceo',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 0
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'execute-chief',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 1
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'execute-chief',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 2
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'producer',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 3
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'producer',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 4
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'producer',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 5
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'producer',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 6
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'manager',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 7
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'manager',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 8
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'manager',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 9
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'manager',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 10
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'manager',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 11
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'manager',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 12
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'manager',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 13
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 2;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'manager',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 14
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 15
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 16
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 17
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 18
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 19
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 20
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 21
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 22
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 23
            ]);
        }

        $faker = Faker\Factory::create();
        $limit = 6000;
        for ($i = 0; $i<$limit; $i++) {
            DB::table ('catalogs')->insert([
                'name' => $faker->name,
                'position' => 'seller',
                'employment' => $faker->dateTimeBetween('-5 years', 'now', 'Europe/Kiev'),
                'salary' => $faker->numberBetween(400,3500),
                'chief_id' => 24
            ]);
        }
    }
}
