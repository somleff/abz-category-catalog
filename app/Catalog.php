<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Catalog extends Model
{
    protected $fillable = ['name', 'position', 'employment', 'salary', 'chief_id'];


    //  One-to-Many Relationships with DB
    public function childs()
    {
        return $this->hasMany(Catalog::class, 'chief_id');
    }
}
