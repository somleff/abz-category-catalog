<?php

namespace App\Http\Controllers;

use App\Catalog;
use Illuminate\Http\Request;

class CatalogController extends Controller
{
    public function search(Request $request)
    {
        $query = $request->input('query');
        $employee = Catalog::where('name', 'LIKE', '%'.$query.'%')
                ->orWhere('position', 'LIKE', '%'.$query.'%')
                ->orWhere('employment', 'LIKE', '%'.$query.'%')
                ->orWhere('salary', 'LIKE', '%'.$query.'%')
                ->paginate(11);
        return view('catalogs.search')->withDetails($employee)->withQuery($query);
    }


    public function getTreeView()
    {
        $catalogs = Catalog::where('chief_id', '=', 0)->with('childs')->get();
        return view ('catalogs.treeview', compact('catalogs'));
    }


    //  Display all information from the database as a list with pagination and sorting
    public function index(Request $request)
    {
        $sort = $request->get('sort', null);

        $employees = Catalog::when(request('sort'), function ($query) use ($sort) {
            $query->orderBy($sort, 'desc');
        })->paginate(11);

        return view ('catalogs.listview', compact('employees'));
    }


    public function create()
    {
        return view('employees.create');
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|min:3',
            'position' => 'required',
            'employment' => 'required|max:10',
            'salary' => 'required|max:4',
            'chief_id' => 'required',
        ]);

        Catalog::create($request->all());
        return redirect()->route('list.index')->with('status','Employee created successfully');
    }


    public function show($id)
    {
        $employee = Catalog::find($id);
        return view('employees.show',compact('employee'));
    }


    public function edit($id)
    {
        $employee = Catalog::find($id);
        return view('employees.edit',compact('employee'));
    }


    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required|min:3',
            'position' => 'required',
            'employment' => 'required|max:10',
            'salary' => 'required|max:4',
            'chief_id' => 'required',
        ]);

        Catalog::find($id)->update($request->all());
        return redirect()->route('list.index')->with('status','Employee updated successfully');
    }


    public function destroy($id)
    {
        Catalog::find($id)->delete();
        return redirect()->route('list.index')->with('status','Employee deleted successfully');
    }
}
