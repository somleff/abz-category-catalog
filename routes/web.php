<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/
Auth::routes();

Route::view('/', 'start');

Route::get('/search', 'CatalogController@search');

Route::get('/tree', 'CatalogController@getTreeView');

Route::resource('/list','CatalogController');
